import 'bootstrap-sass'
import anime from 'animejs'
import 'animate.css'
import LazyLoad from 'vanilla-lazyload'

import './scss/main.scss'

var myLazyLoad = new LazyLoad()

$.when($.ready).then(() => {
    /**
     * ELEMENTS
     */
    let scrollTopButton = $('.scroll-top')
    let secondaryNav = $('.secondary-navigation')

    /**
     * CLICK event listeners
     */
    // scroll top
    scrollTopButton.click(() => {
        $('body').animate({ scrollTop: 0 }, 500)
    })

    // open close navigation
    $('.open-secondary-nav').click(() => {
        secondaryNav.removeClass('fadeOutDown').addClass('fadeIn').show()
        $('body').css('overflow', 'hidden')
    })
    $('.close-secondary-nav').click(() => {
        $('body').css('overflow', 'auto')
        secondaryNav.removeClass('fadeIn').addClass('fadeOutDown')
        setTimeout(() => secondaryNav.hide(), 500)
    })

    // scroll on navigation button clicks
    $('nav a[href^="#"]').click(function () {
        $('.container.app').animate({ scrollTop: $(this.hash).offset().top }, 500);
    })

    $('.secondary-navigation a[href^="#"]').click(function () {
        $('.close-secondary-nav').click();
        $('.container.app').animate({ scrollTop: $(this.hash).offset().top }, 500);
    })

    /**
     * ON WINDOW SCROLL events
     */
    $(window).scroll(() => {
        // show hide scroll top button
        if ($(window).scrollTop() > $(window).height()) {
            scrollTopButton.removeClass('zoomOut').addClass('zoomIn').show()
        } else {
            scrollTopButton.removeClass('zoomIn').addClass('zoomOut').hide(500)
        }
    })
    $(window).scroll()
})