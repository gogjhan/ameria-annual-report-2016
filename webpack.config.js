let webpack = require('webpack')
let HtmlWebpackPlugin = require('html-webpack-plugin');
let FaviconsWebpackPlugin = require('favicons-webpack-plugin')
let CopyWebpackPlugin = require('copy-webpack-plugin');
let ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
    entry: './index.js',
    output: {
        path: __dirname + '/dist',
        filename: 'index_bundle.js'
    },
    module: {
        loaders: [
            { test: /\.pug/, loader: 'pug-loader' },
            // { test: /\.(scss|css|sass|less)$/, loader: ['style-loader', 'css-loader', 'sass-loader']},
            { test: /\.(scss|css|sass|less)$/, loader: ExtractTextPlugin.extract({
                    fallbackLoader: "style-loader",
                    loader: "css-loader!sass-loader",
                }),},
            { test: /\.(png|svg|jpe?g|gif)$/, loader: 'file-loader' },
            { test: /\.(woff|woff2|eot|ttf|otf)$/, loader: 'file-loader' }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './index.pug'
        }),
        new FaviconsWebpackPlugin('./images/logo.png'),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        //copy images and fonts to public folder for serving statically
        new CopyWebpackPlugin([
            { from: 'images', to: './images' },
            { from: 'files', to: './files' },
            { from: 'fonts', to: './fonts'}
        ]),
        new ExtractTextPlugin('./main.css')
    ]
};